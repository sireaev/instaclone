import {  Component,  HostListener,  OnInit} from '@angular/core';
import {  Inject} from '@angular/core';
import {  DOCUMENT} from '@angular/platform-browser';
import { ModalPhotoComponent } from './modal-photo/modal-photo.component';

import {  PhotosService} from './core/_services/photos.service';
import { BsModalRef ,BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  searchText: string = 'Search';
  activeSearch: boolean = false;
  navigatorSlideUp: boolean = false;
  loading: boolean = false;
  photos = []
  organization: object = {
    name: 'ebsintegrator',
    description: 'EBS Integrator \'iOS App Development \'Android App Development \'Web Development',
    image: '../assets/images/ebs.jpg',
    link: 'www.ebs-integrator.com',
    statistics: {
      posts: 3005,
      followers: 15420,
      following: 2153
    }
  }
  queryParams = {
    page: 0,
    perPage: 50
  }

  constructor(@Inject(DOCUMENT) private document: Document,
    private photoProvider: PhotosService,
    public bsModalRef: BsModalRef,
    public modalService: BsModalService) {}

  ngOnInit() {
    this.retrievePhotos();
  }
  private switchMask(event) {
    this.activeSearch = !this.activeSearch;
    this.searchText = event.target && event.target.value ? event.target.value : 'Search';
  }

  private onClickedOutside() {
    this.activeSearch = false;
  }

  private onScroll() {
    this.retrievePhotos()
  }

  private openPhotoModal(i) {
    const initialState = {
      currentPhoto: this.photos[i].urls.raw,
      photos: this.photos,
      index: i,
      organization: this.organization
    }
    this.bsModalRef = this.modalService.show(ModalPhotoComponent, Object.assign({initialState}, { class: 'modal-lg' }));
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  private retrievePhotos() {
    if (this.loading) {
      return;
    }
    this.loading = true;
    this.photoProvider.retrievePhotos(this.queryParams.page, this.queryParams.perPage, 'latest').then(response => {
        console.log(response);
        this.photos.push(...response);
        this.queryParams.page = this.queryParams.page + 1;
        setTimeout(() => {
          this.loading = false;
        },1500)
    });
  }

  private parseBackground(backgroundURL) {
    return {
      "background": `url(${backgroundURL}) no-repeat center center`
    }
  }

  @HostListener('window:scroll', ['$event', '$window'])
  private onWindowScroll($event) {
    if (document.documentElement.scrollTop >= 100) {
      this.navigatorSlideUp = true;
    } else {
      this.navigatorSlideUp = false;
    }
  }
}
