import { Injectable } from '@angular/core';
import Unsplash, { toJson } from 'unsplash-js';
import * as config from '../../../unsplash.config.json';

@Injectable()
export class PhotosService {
  unsplash = new Unsplash(config);
  constructor() { }

  retrievePhotos(page, perPage, orderBy) {
    return this.unsplash.photos.listPhotos(page, perPage, orderBy).then(toJson);
  }
}
