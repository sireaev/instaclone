import { Component, OnInit } from '@angular/core';
import { BsModalRef,ModalModule ,BsModalService } from 'ngx-bootstrap';

@Component({
  selector: 'app-modal-photo',
  templateUrl: './modal-photo.component.html',
  styleUrls: ['./modal-photo.component.scss']
})
export class ModalPhotoComponent implements OnInit {
  photos = [];
  index: any;
  currentPhoto: any;
  organization: any;
  constructor(public bsModalRef: BsModalRef) {}

  ngOnInit() {
  }

  spinPhoto(nextPhoto) {
    if (nextPhoto) {
      this.index < this.photos.length ? this.index++ : this.index;
    } else {
      this.index ? this.index-- : this.index;
    }
    this.currentPhoto = this.photos[this.index].urls.raw;
  }

}
