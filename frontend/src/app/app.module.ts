import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ClickOutsideModule } from 'ng-click-outside';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ModalModule } from 'ngx-bootstrap/modal';

import { AppComponent } from './app.component';

import { PhotosService } from './core/_services/photos.service';
import { ModalPhotoComponent } from './modal-photo/modal-photo.component';
import { BsModalRef ,BsModalService } from 'ngx-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    ModalPhotoComponent
  ],
  entryComponents: [ModalPhotoComponent],
  imports: [
    BrowserModule,
    ClickOutsideModule,
    InfiniteScrollModule,
    ModalModule.forRoot()
  ],
  providers: [PhotosService, BsModalRef, BsModalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
