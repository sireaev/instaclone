# Frontend

#First time initialization

- Run command `cd frontend && npm i`

#How to start frontend

- Go to frontend folder `cd frontend` and run `npm start`
- Website will be available at http://localhost:4200

# Backend (backend task)

### Software to be installed

* MySQL Server
* MySQL Workbench
* Node v.8
* Git

### Set up your database credentials and host in `backend/config-development.json` and `backend/config-production.json` for production

### Install packages and run server and frontend

- Run MySQL Server
- Go to project folder in terminal
- Run command `cd dashboard && npm i && npm start` to run frontend dashboard, being in project folder
- Run command `cd ../backend && npm run init`, being in dashboard folder, this will run server on production mode
- To run development mode being in project folder run `cd dashboard && npm start && cd ../backend && npm run start-dev`

> Server should run on `http://localhost:8000`
> Dashboard is available at `http://localhost:4200` and it is running on Angular 6
> To get login as administrator add your email's administrator in `backend/config-development.json` if you're running development or `backend/config-production.json` for production