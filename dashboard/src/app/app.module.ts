import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { MatGridListModule, MatCardModule, MatMenuModule, MatIconModule, MatButtonModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';

import { AppComponent } from './app.component';
import { ArticlesComponent } from './articles/articles.component';
import { LoginComponent } from './login/login.component';

import { RequestService } from './core/_services/request.service';
import { UserService } from './core/_services/user.service';
import { AuthGuardService } from './core/_guards/auth-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    ArticlesComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    MatTableModule
  ],
  providers: [RequestService, UserService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
