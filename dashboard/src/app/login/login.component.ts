import { Component,  OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../core/_services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isRegister: boolean = false;
  loginForm: FormGroup;
  registerForm: FormGroup;
  isFinish: boolean = false;
  constructor(private fb: FormBuilder,
  private router: Router,
  private user: UserService) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.loginForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.min(2), Validators.max(25)])]
    });
    this.registerForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      fullName: ['', Validators.compose([Validators.required])],
      passwords: this.fb.group({
        password: ['', Validators.required],
        confirmPassword: ['', Validators.required]
      }, {validator: this.matchPasswords})
    });
  }

  matchPasswords(group: FormGroup) {
    var valid = false;

    for (let name in group.controls) {
      var val = group.controls[name].value
      console.log(val);
    }

    if (valid) {
      return null;
    }

    return {
      mismatch: true
    };
  }

  login() {
    this.isFinish = true;
    console.log('REQUEST', this.loginForm.value);
    return this.user.login(this.loginForm.value).then(response => {
      console.log(response);
      if (response) {
        this.router.navigate(['/articles']);
        this.isFinish = false;
      }
    }).catch(error => {
      this.isFinish = false;
      console.log(error);
    })
  }

  register() {
    this.isFinish = true;
    let payload = {
      email: this.registerForm.value.email,
      fullName: this.registerForm.value.fullName,
      password: this.registerForm.value.passwords.password
    }
    console.log('REQUEST', payload);
    return this.user.register(payload).then(response => {
      console.log(response);
      this.isRegister = false;
      this.isFinish = false;
    }).catch(error => {
      console.log(error);
      this.isFinish = false;
    })
  }
}
