import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private request: RequestService,
  private router: Router) { }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('token');

    return !!token;
  }

  public login(data: any) {
    return this.request.post('auth/login', data).toPromise().then((response: any) => {
      localStorage.setItem('token', response.data.token);
      return Promise.resolve(true);
    })
  }

  public logout() {
    localStorage.removeItem('token');
    return this.router.navigate(['/login']);
  }

  public register(data?: any) {
    return this.request.post('auth/register', data).toPromise();
  }

  public retrieveArticleList(data?: any) {
    return this.request.get('article/list', data).toPromise();
  }

  public likeArticle(data?: any) {
    return this.request.post('like', data).toPromise();
  }

  public unlikeArticle(data?: any) {
    return this.request.delete('unlike', data).toPromise();
  }

  public retrieveUser(data?: any) {
    return this.request.get('user', data).toPromise();
  }

  public retrieveUsers(data?: any) {
    return this.request.get('users', data).toPromise();
  }
}
