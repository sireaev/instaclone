import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import * as config from '../../../config.json';

@Injectable()
export class RequestService {
  headers = new HttpHeaders({
    'Content-Type': 'application/json;charset=utf-8'
  });

  constructor(public http: HttpClient) {}

  get(endpoint: string, params?: any, reqOpts?: any) {
    if (!reqOpts) {
      reqOpts = {
        params: new HttpParams()
      };
    }

    // Support easy query params for GET requests
    reqOpts.params = new HttpParams();
    for (const k in params) {
      if (params) {
        reqOpts.params = reqOpts.params.set(k, params[k]);
      }
    }
    reqOpts.headers = new HttpHeaders({
      'Cache-Control': 'no-cache',
      'Pragma': 'no-cache',
      'Expires': 'Sat, 01 Jan 2000 00:00:00 GMT',
      'x-token': localStorage.getItem('token')
    });
    reqOpts.headers = this._clearObject(reqOpts.headers);
    return this.http.get((<any>config).url + '/' + endpoint, reqOpts);
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    if (!reqOpts) {
      reqOpts = {
        headers: new HttpHeaders()
      };
    }
    reqOpts.headers = Object.assign({}, reqOpts.headers, this.headers, {'x-token': localStorage.getItem('token')});
    reqOpts.headers = this._clearObject(reqOpts.headers);

    return this.http.post((<any>config).url + '/' + endpoint, body, reqOpts);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    if (!reqOpts) {
      reqOpts = {
        headers: new HttpHeaders()
      };
    }
    reqOpts.headers = Object.assign({}, reqOpts.headers, this.headers, {'x-token': localStorage.getItem('token')});
    reqOpts.headers = this._clearObject(reqOpts.headers);

    return this.http.put((<any>config).url + '/' + endpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    if (!reqOpts) {
      reqOpts = {
        headers: new HttpHeaders()
      };
    }
    let body = {};
    for (let param in reqOpts) {
      body[param] = reqOpts[param];
    }
    reqOpts.body = body;
    reqOpts.headers = Object.assign({}, reqOpts.headers, this.headers, {'x-token': localStorage.getItem('token')});
    reqOpts.headers = this._clearObject(reqOpts.headers);

    return this.http.delete((<any>config).url + '/' + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    if (!reqOpts) {
      reqOpts = {
        headers: new HttpHeaders()
      };
    }
    reqOpts.headers = Object.assign({}, reqOpts.headers, this.headers, {'x-token': localStorage.getItem('token')});
    reqOpts.headers = this._clearObject(reqOpts.headers);

    return this.http.patch((<any>config).url + '/' + endpoint, body, reqOpts);
  }

  _clearObject(data: any) {
      for (const index in data) {
          if (!data.hasOwnProperty(index) && typeof data[index] !== 'undefined') {
              continue;
          }

          if (data[index] === null || data[index] === undefined) {
              delete data[index];
          }
      }

      return data;
  }
}
