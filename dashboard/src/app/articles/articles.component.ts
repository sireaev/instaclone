import { Component, OnInit } from '@angular/core';
import { UserService } from '../core/_services/user.service';

@Component({
  selector: 'articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit{
  cardParams = { 
    cols: 2,
    rows: 1
  }
  cards = [];
  liked: boolean = false;
  noData:string = "There are no articles"
  isLoading: boolean = true;
  currentUser: any;
  users = [];
  displayedColumns = ['index', 'name', 'email', 'registered'];
  constructor(private user: UserService) {}

  ngOnInit() {
    this.retrieveUser();
    this.retrieveArticles();
  }

  retrieveUser() {
    this.user.retrieveUser().then(response => this.currentUser = response).then(() => {
      if (this.currentUser && this.currentUser.admin) {
        this.user.retrieveUsers().then((response: any) => {
          console.log('USER LIST',response);
          this.users = response;
        });
      }
      console.log(this.currentUser);
    });
  }

  likeArticle(liked, articleId) {
    if (!liked) {
      return this.user.likeArticle({articleId}).then((response: any) => {
        if (response && !response.liked) {
          return this.retrieveArticles();
        }
      })
    }
  }

  unlikeArticle(liked, articleId) {
    if (liked) {
      console.log({articleId})
      return this.user.unlikeArticle({articleId}).then((response: any) => {
          return this.retrieveArticles();
      })
    }
  }

  logout() {
    return this.user.logout();
  }

  retrieveArticles() {
    this.isLoading = true;
    return this.user.retrieveArticleList({}).then((response: any) => {
      if (response) {
        this.cards = [];
        response.map(article => this.cards.push(Object.assign({}, this.cardParams, article)))
        console.log(this.cards);
      }
        this.isLoading = false;
    }, error => {
      console.log(error);
      this.isLoading = false;
    })
  }
}
