let mysql = require('mysql2');
let fs = require('fs');

let config = null;

try {
    config = JSON.parse(fs.readFileSync('./config-development.json', 'utf8'));
} catch(error) {
    console.log('NOW CONFIG FILE PRESENT!');
    throw error;
}

let connection = mysql.createConnection({
    host: config.database.host,
    user: config.database.user,
    password: config.database.password
});

connection.connect((error) => {
    if (error) {
        console.log('PLEASE MAKE SURE DATABASE CONNECTION DATA IS CORRECT');
        throw error;
    }

    connection.query(`create database if not exists ${config.database.name};`, (error) => {
        if (error) {
            console.log('DATABASE CREATION FAILED');
            throw error;
        }

        console.log('DATABASE SUCCESSFULLY CHECKED');
        connection.close();
    });
});
