let fs = require('fs-extra');
let path = require('path')

let moduleName = process.argv[2];
let modelName = process.argv[3];

if (!moduleName || !modelName) process.exit(1);

let files = {
    models: {
        file: fs.readFileSync(path.join(__dirname, './files/model'), 'utf8'),
        name: modelName
    },
    dao: {
        file: fs.readFileSync(path.join(__dirname, './files/dao'), 'utf8'),
        name: modelName + 'Dao'
    },
    services: {
        file: fs.readFileSync(path.join(__dirname, './files/service'), 'utf8'),
        name: modelName + 'Service'
    },
    facade: {
        file: fs.readFileSync(path.join(__dirname, './files/facade'), 'utf8'),
        name: modelName + 'Facade'
    },
    controllers: {
        file: fs.readFileSync(path.join(__dirname, './files/controller'), 'utf8'),
        name: modelName + 'Controller'
    }
}

for (let index in files) {
    if (!files.hasOwnProperty(index)) {
        continue;
    }

    files[index].file = files[index].file.replace(new RegExp('{NameSpace}', 'g'), modelName);

    fs.outputFileSync(path.join(__dirname, '../../lib', moduleName, index, files[index].name + '.js'), files[index].file);
}