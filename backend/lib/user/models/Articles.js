import Sequelize from 'sequelize';
import database from '../../database';
import Likes from './Likes';
import Users from './Users';

const Articles = database.define('articles', {
    id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4
    },
    user_id: {
        type: Sequelize.UUID,
        allowNull: false
    },
    topic: {
        type: Sequelize.STRING,
        allowNull: false
    },
    content: {
        type: Sequelize.TEXT,
        allowNull: false
    }
});

export default Articles;

Articles.hasMany(Likes, {foreignKey: 'article_id'});
Users.hasMany(Articles, {foreignKey: 'user_id'});

Likes.belongsTo(Articles, {foreignKey: 'article_id'});