import Sequelize from 'sequelize';
import database from '../../database';
import sequelize from '../../database';

const Likes = database.define('likes', {
    id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4
    },
    user_id: {
        type: Sequelize.UUID,
        allowNull: false
    },
    article_id: {
        type: Sequelize.UUID,
        allowNull: false
    }
});

export default Likes;
