import Sequelize from 'sequelize';
import database from '../../database';

const Users = database.define('users', {
    id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4
    },
    full_name : {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING(72),
        allowNull: false
    }
});

export default Users;
