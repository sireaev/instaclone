import Articles from '../models/Articles';
import { AbstractDao } from '../../abstractModule/AbstractDao';

export class ArticlesDao extends AbstractDao {
    constructor() {
        super(Articles);
    }

    getArticles() {
        return this.model.findAll({
            include: [{
                all: true,
                nested: true
            }]
        });
    }
}
