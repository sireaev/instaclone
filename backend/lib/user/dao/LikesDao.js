import Likes from '../models/Likes';
import { AbstractDao } from '../../abstractModule/AbstractDao';

export class LikesDao extends AbstractDao {
    constructor() {
        super(Likes);
    }
}
