import Users from '../models/Users';
import { AbstractDao } from '../../abstractModule/AbstractDao';

export class UsersDao extends AbstractDao {
    constructor() {
        super(Users);
    }

    retrieveUserByEmailForLogin(email) {
        let sql = {
            where: {
                email: email
            }
        };

        return this.model.find(sql);
    }

    listUsers() {
        return this.model.findAll();
    }
}
