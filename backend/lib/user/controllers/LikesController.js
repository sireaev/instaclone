import validator from 'express-validation';

import { AbstractController } from '../../abstractModule/AbstractController';
import { LikesFacade } from '../facade/LikesFacade';

export class LikesController extends AbstractController {
    constructor(express) {
        super(express, new LikesFacade());
    }

    like(req, res) {
        return this._makeBodyRequest('like', req, res);
    }

    unlike(req, res) {
        return this._makeBodyRequest('unlike', req ,res);
    }

    get routes() {
        return [
            this.createRoute('POST', '/like', this.like),
            this.createRoute('DELETE', '/unlike', this.unlike)
        ];
    }
}
