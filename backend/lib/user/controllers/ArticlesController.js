import validator from 'express-validation';

import { AbstractController } from '../../abstractModule/AbstractController';
import { ArticlesFacade } from '../facade/ArticlesFacade';

export class ArticlesController extends AbstractController {
    constructor(express) {
        super(express, new ArticlesFacade());
    }

    listArticles(req, res) {
        return this._makeQueryRequest('listArticles', req, res);
    }

    /**
     * @api {get} /article/list Request Articles list
     * @apiName GetArticles
     * @apiGroup User
     * @apiHeader {String} x-token Users unique token key.
     *
     * @apiSuccess {String} id Id of the article.
     * @apiSuccess {String} userId Id of the User.
     * @apiSuccess {String} topic Article's topic.
     * @apiSuccess {String} content Article's content.
     * @apiSuccess {Number} likes Article's like number
     * @apiSuccess {Boolean} liked User's current like state.
     * @apiSuccess {Date} createdAt Article's creation date.
     * @apiSuccess {Date} updatedAt Article's last update.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     [
     *      ...
     *      {
     *          "id": "6e4f927e-77d4-11e8-adc0-fa7ae01bbebc",
     *          "userId": "1e4f927e-77d4-11e8-adc0-fa7ae01bbebc",
     *          "topic": "Topic example",
     *          "content": "Content herer...",
     *          "likes": 10,
     *          "liked": true,
     *          "createdAt": "2018-06-24T17:32:13.327",
     *          "updatedAt": "2018-06-24T17:32:13.327"
     *      }
     *      ]
     *
     * @apiError ArticleNotFound There are no articles.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "message": 'NOT_FOUND',
     *       "status": 404
     *     }
     */

    get routes() {
        return [
            this.createRoute('GET', '/article/list', this.listArticles)
        ];
    }
}