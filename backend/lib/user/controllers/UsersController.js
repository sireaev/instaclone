import validator from 'express-validation';

import { AbstractController } from '../../abstractModule/AbstractController';
import { UsersFacade } from '../facade/UsersFacade';

export class UsersController extends AbstractController {
    constructor(express) {
        super(express, new UsersFacade());
    }

    retrieveUser(req, res) {
        return this._makeQueryRequest('retrieveUser', req ,res);
    }

    listUsers(req, res) {
        return this._makeQueryRequest('listUsers', req, res);
    }

    get routes() {
        return [
            this.createRoute('GET', '/user', this.retrieveUser),
            this.createRoute('GET', '/users', this.listUsers)
        ];
    }
}
