import { UsersService } from '../services/UsersService';

import errors from '../../errors';

import { AbstractFacade } from '../../abstractModule/AbstractFacade';

export class UsersFacade extends AbstractFacade {
    constructor() {
        super(UsersService);
    }

    retrieveUser(data, user) {
        if (user.id) {
            return this.service.retrieveUserById(user.id).then(response => {
                if (!response) return Promise.reject(errors.NOT_FOUND);

                return Promise.resolve(this._transformFromUser(response, user));
            }).catch(error => {
                return Promise.reject(errors.FATAL_ERROR);
            });
        } else {
            return this.service.retrieveUserByEmail(user.email).then(response => {
                if (!response) return Promise.reject(errors.NOT_FOUND);
    
                return Promise.resolve(this._transformFromUser(response, user));
            }).catch(error => {
                return Promise.reject(errors.FATAL_ERROR);
            });
        }
    }

    listUsers(data, user) {
        console.log('user', user);
        if (user && !user.admin) {
            return Promise.reject(errors.ACCESS_DENIED);
        }

        return this.service.listUsers().then(response => {
            return response && response.length ? response.map(item => this._transformFromUser(item, user)) : [];
        })
    }

    _transformFromUser(data, user) {
        return {
            id: data.id,
            fullName: data.full_name,
            email: data.email,
            admin: user.admin,
            createdAt: data.createdAt,
        }
    }
}
