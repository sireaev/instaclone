import { LikesService } from '../services/LikesService';

import errors from '../../errors';

import { AbstractFacade } from '../../abstractModule/AbstractFacade';

export class LikesFacade extends AbstractFacade {
    constructor() {
        super(LikesService);
    }

    like(data, user) {
        return this.service.retrieveLike(data.articleId, user.id).then(response => {
            if (response) {
                return Promise.resolve({liked: true});
            }

            return this.service.like(this._transformToLike(data, user));
        })
    }

    unlike(data, user) {
        return this.service.retrieveLike(data.articleId, user.id).then(response => {
            if (!response) {
                return Promise.reject(errors.NOT_FOUND);
            }

            return this.service.unlike(this._transformToLike(data, user));
        }).then(() => Promise.resolve())
    }

    _transformToLike(data, user) {
        return {
            user_id: user.id,
            article_id: data.articleId
        }
    }
}
