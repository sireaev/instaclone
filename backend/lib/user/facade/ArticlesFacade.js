import { ArticlesService } from '../services/ArticlesService';

import errors from '../../errors';

import { AbstractFacade } from '../../abstractModule/AbstractFacade';

export class ArticlesFacade extends AbstractFacade {
    constructor() {
        super(ArticlesService);
    }

    listArticles(data, user) {
        return this.service.listArticles().then(response => {
            if (!response) {
                return Promise.reject(errors.NOT_FOUND);
            }

            return response.map(article => this._transformFromArticles(article, user));
        })
    }

    _transformFromArticles(data, user) {
        return {
            id: data.id,
            userId: data.user_id,
            topic: data.topic,
            content: data.content,
            likes: data.likes ? data.likes.length : null,
            liked: data.likes && data.likes.length ? data.likes.some(like => like.user_id === user.id) : null,
            createdAt: data.createdAt,
            updatedAt: data.updatedAt
        }
    }
}
