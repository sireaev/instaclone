import { UsersDao } from '../dao/UsersDao';
import { AbstractService } from '../../abstractModule/AbstractService';

export class UsersService extends AbstractService {
    constructor() {
        super(UsersDao);
    }

    createUser(user, raw = false) {
        return this.dao.createEntity(user).then(user => (!user || raw) ? user : user.get());
    }

    retrieveUserByEmail(email, raw = false) {
        return this.dao.retrieveEntityBy('email', email).then(user => (!user || raw) ? user : user.get());
    }

    retrieveUserById(user_id) {
        return this.dao.retrieveEntityById(user_id);
    }

    retrieveUserByEmailForLogin(email) {
        return this.dao.retrieveUserByEmailForLogin(email);
    }

    listUsers() {
        return this.dao.listUsers();
    }
}
