import { ArticlesDao } from '../dao/ArticlesDao';
import { AbstractService } from '../../abstractModule/AbstractService';

export class ArticlesService extends AbstractService {
    constructor() {
        super(ArticlesDao);
    }

    listArticles() {
        return this.dao.getArticles();
    }
}
