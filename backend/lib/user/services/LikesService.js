import { LikesDao } from '../dao/LikesDao';
import { AbstractService } from '../../abstractModule/AbstractService';

export class LikesService extends AbstractService {
    constructor() {
        super(LikesDao);
    }

    retrieveLike(article_id, user_id) {
        return this.dao.retrieveEntityBy('article_id', article_id, user_id);
    }

    like(data) {
        return this.dao.createEntity(data);
    }

    unlike(data) {
        return this.dao.deleteEntityBy('article_id', data.article_id, data.user_id);
    }
}
