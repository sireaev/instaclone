import Sequelize from 'sequelize';
import fs from 'fs';
import path from 'path';

let config = JSON.parse(fs.readFileSync(path.join(__dirname, `../config-${process.env.NODE_ENV || 'prod'}.json`)));
let databaseData = config.database;

const sequelize = new Sequelize(databaseData.name, databaseData.user, databaseData.password, {
    host: databaseData.host,
    dialect: 'mysql',
    pool: {
        max: 10,
        min: 0,
        idle: 10000
    },
    logging: false,
    operatorsAliases: false
});

export default sequelize;
