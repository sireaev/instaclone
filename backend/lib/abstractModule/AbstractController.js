import errors from '../errors';

export class AbstractController {
    constructor(express, facade, preInit) {
        this.express = express;
        this.facade = facade;
        
        if (preInit) {
            preInit.bind(this)();
        } 

        this.routes.forEach((route) => {
            if (route.handlers) {
                return express[route.method](route.path, ...route.handlers);
            }

            if (route.validator) {
                return express[route.method](route.path, route.validator, route.handler);
            }

            return express[route.method](route.path, route.handler);
        });
    }

    /**
     * This method leaves the user to decide how much handlers he wants to use
     * @param {string} method 
     * @param {string} path 
     * @param {array} handlers - a list of handlers 
     */
    createRoute(method, path, ...handlers) {
        return {
            method: method.toLowerCase(),
            path: path.toLowerCase(),
            handlers: handlers.map(handler => handler.bind(this))
        }
    }

    buildSuccess(data) {
        return { data };
    }

    buildError(error) {
        return { error };
    }

    get routes() {
        return [];
    }

    _makeQueryRequest(name, req, res, rawReq = false) {
        return this._makeRequest(name, req, res, rawReq, 'query');
    }

    _makeBodyRequest(name, req, res, rawReq = false) {
        return this._makeRequest(name, req, res, rawReq, 'body');
    }

    _makeRequest(name, req, res, rawReq, payloadType) {
        let parsedRequest = rawReq ? req : req[payloadType];

        return this.facade[name](parsedRequest, req.loggedUser).then((response) => {
            if (!response) {
                return res.status(204).send();
            }

            return res.send(response);
        }).catch(error => {
            if (!error.status) {
                console.log(error);
                error = errors.FATAL_ERROR;
            }
            return res.status(error.status || 500).json(this.buildError({ message: error.message}));
        });
    }
}
