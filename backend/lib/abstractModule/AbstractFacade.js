export class AbstractFacade {
    constructor(Service, Dao) {
        this.service = new Service(Dao);
    }

    _clearObject(obj) {
        for (let index in obj) {
            if (!obj.hasOwnProperty(index) && typeof obj[index] !== 'undefined') {
                continue;
            }

            if (obj[index] === null || obj[index] === undefined) {
                delete obj[index];
            }
        }

        return obj;
    }
}
