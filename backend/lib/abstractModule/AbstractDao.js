export class AbstractDao {
    constructor(model) {
        this.model = model;
    }

    createEntity(entity) {
        return this.model.create(entity);
    }

    retrieveEntityById(id) {
        return this.model.findById(id);
    }

    retrieveEntityBy(field, data, user_id) {
        let sql = {
            where: {
                [field]: data
            }
        };

        if (user_id) {
            sql.where.user_id = user_id;
        }

        return this.model.find(sql);
    }

    deleteEntityBy(field, value, user_id) {
        let sql = {
            where: {
                [field]: value
            }
        };

        if (user_id) {
            sql.where.user_id = user_id;
        }

        return this.model.destroy(sql);
    }
}
