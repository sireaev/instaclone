import fs from 'fs';

let config = null;

try {
    let configString = fs.readFileSync(`./config-${process.env.NODE_ENV || 'prod'}.json`);
    config = JSON.parse(configString);
    process.env.CONFIG = configString;
} catch(error) {
    console.log(error);
    console.log('PLEASE MAKE SURE THERE IS CONFIG FILE IN BACKEND ROOT FOLDER');
    process.exit(1);
}

export default config;