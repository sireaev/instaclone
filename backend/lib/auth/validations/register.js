import Joi from 'joi';

export default {
    body: {
        email: Joi.string().email().required(),
        password: Joi.string().min(2).max(25).required(),
        fullName: Joi.string().required()
    }
}