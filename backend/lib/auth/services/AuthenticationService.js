import { UsersService } from '../../user/services/UsersService';

import errors from '../../errors';
import bcrypt from 'bcrypt';
import moment from 'moment';

export class AuthenticationService {
    constructor() {
        this.config = JSON.parse(process.env.CONFIG);
        this.usersService = new UsersService();
    }

    register(data) {
        return this.usersService.retrieveUserByEmail(data.email).then(user => {
            if (user) {
                return Promise.reject(errors.EMAIL_ALREADY_REGISTERED);
            }

            return Promise.resolve(bcrypt.hash(data.password, this.config.password.saltRounds))
        }).then(password => {
            let user = {
                email: data.email,
                password: password,
                full_name: data.fullName
            }

            return Promise.resolve(this.usersService.createUser(user));
        }).catch(error => {
            if (error.status) {
                return Promise.reject(error)
            }

            return Promise.reject(errors.FATAL_ERROR);
        });
    }

    login(data) {
        let email = data.email;
        let password = null;
        let promises = null;

        return this.usersService.retrieveUserByEmailForLogin(email).then(user => {
            if (!user) {
                return Promise.reject(errors.INVALID_CREDENTIALS);
            }

            promises = [
                bcrypt.compare(data.password, user.password),
                Promise.resolve(user)
            ];

            return Promise.all(promises);
        }).then(([isEqual, user]) => {
            if (!isEqual) {
                return Promise.reject(errors.INVALID_CREDENTIALS);
            }

            let payload = {
                id: user.id,
                email: user.email,
                fullName: user.full_name
            };

            if (this.config.admins &&
                this.config.admins.length &&
                this.config.admins.find(admin => admin.email === user.email)) {
                payload.admin = true;
            }

            return Promise.resolve(payload);
        });
    }

    verify(token) {
        return this.usersService.verify(token);
    }
}