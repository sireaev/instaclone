import { AbstractController } from '../../abstractModule/AbstractController';
import { AuthenticationService } from '../services/AuthenticationService';

import errors from '../../errors';
import validator from 'express-validation';
import moment from 'moment';

import authenticateValidation from '../validations/authenticate';
import registerValidation from '../validations/register';
import jwt from 'jsonwebtoken';

export class AuthController extends AbstractController {
    constructor(express) {
        let preInit = function () {
            express.use(this.checkToken.bind(this));
        };

        super(express, new AuthenticationService(), preInit);

        this.authenticationService = this.facade;
    }

    authenticate(req, res) {
        return this.authenticationService.login(req.body).then(data => {
            let token = jwt.sign(data, this.express.get('secretKey'), {expiresIn: '6h'});

            res.json(this.buildSuccess({ token }));
        }).catch(error => {
            res.status(error.status >= 100 && error.status < 600 ? error.status : 500).json(this.buildError({
                message: error.message
            }));
        });
    }

    register(req, res) {
        this.authenticationService.register(req.body).then(response => {
            res.status(204).send();
        }).catch(error => {
            res.status(error.status >= 100 && error.status < 600 ? error.status : 500).json(this.buildError({
                message: error.message
            }));
        });
    }

    checkToken(req, res, next) {
        if (req.path.indexOf('/auth') === 0) {
            return next();
        }
        let token = req.get("x-token");

        if (!token || token === 'null') {
            return res.status(errors.UNAUTHORIZED.status).send(this.buildError({
                    message: errors.UNAUTHORIZED.message
                }));
        }

        jwt.verify(token, this.express.get('secretKey'), (error, user) => {
            if (error) {
                error = error.name === 'TokenExpiredError' ? errors.TOKEN_EXPIRED : errors.INVALID_CREDENTIALS;
                return res.status(error.status >= 100 && error.status < 600 ? error.status : 500).send(this.buildError({
                    message: error.message
                }));
            }

            if (!user.id) {
                return res.status(errors.INVALID_CREDENTIALS.status).send(this.buildError({
                    message: errors.INVALID_CREDENTIALS.message
                }));
            }

            req.loggedUser = user;

            next()
        });
    }

    get routes() {
        return [
            this.createRoute('POST', '/auth/login', validator(authenticateValidation), this.authenticate),
            this.createRoute('POST', '/auth/register', validator(registerValidation), this.register)
        ];
    }

    get tokenSkip() {
        return [
            '/auth/register',
            '/auth/login'
        ]
    }
}