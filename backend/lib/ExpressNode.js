export class ExpressNode {
    constructor(express) {
        this.express = express();

        this._controllers = [];
    }

    use(...args) {
        this.express.use(...args);
    }

    set(...args) {
        this.express.set(...args);
    }

    addControllers(controllers) {
        for (let index in controllers) {
            if (!controllers.hasOwnProperty(index)) {
                continue;
            }

            let Controller = controllers[index];

            this._controllers.push(new Controller(this.express));
        }
    }

    startServer() {
        this.express.listen(process.env.PORT);
    }
}
