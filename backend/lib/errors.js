export default {
    INVALID_CREDENTIALS: {
        message: 'INVALID_CREDENTIALS',
        status: 401
    },
    EMAIL_ALREADY_REGISTERED: {
        message: 'EMAIL_ALREADY_REGISTERED',
        status: 400
    },
    NOT_FOUND: {
        message: 'NOT_FOUND',
        status: 404
    },
    FATAL_ERROR: {
        message: 'FATAL_ERROR',
        status: 500
    },
    ACCESS_DENIED: {
        message: 'ACCESS_DENIED',
        status: 403
    },
    INVALID_TOKEN: {
        message: 'INVALID_TOKEN',
        status: 401
    },
    UNAUTHORIZED: {
        message: "UNAUTHORIZED",
        status: 401
    },
    TOKEN_EXPIRED: {
        message: "TOKEN_EXPIRED",
        status: 401
    }
}
