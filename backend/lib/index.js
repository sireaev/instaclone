import express from 'express';
import bodyParser from 'body-parser';
import database from './database';
import config from './config';

import { ExpressNode } from './ExpressNode';

import * as usersControllers from './user/controllers';
import * as authControllers from './auth/controllers';

let expressNode = new ExpressNode(express);

expressNode.set('config', config);
expressNode.set('secretKey', config.password.secretKey);

expressNode.use(bodyParser.urlencoded({extended: true}));
expressNode.use(bodyParser.json());

expressNode.use(function (req, res, next) {
    let origins = [
        process.env.FRONTEND_LINK
    ];

    let origin = req.headers.origin;
    if (origins.indexOf(origin) > -1) {
        res.setHeader('Access-Control-Allow-Origin', origin);
    }

    res.header("Access-Control-Allow-Origin", process.env.FRONTEND_LINK);
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-token, Pragma, Cache-control, Expires");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");

    if (req.method === "OPTIONS") {
        return res.status(200).end();
    }

    next();
});

expressNode.addControllers([
    ...Object.values(authControllers),
    ...Object.values(usersControllers)
]);

expressNode.use(function (err, req, res, next) {
    console.log(err);
    res.status(400).json(err);
});


database.sync().then(() => {
    expressNode.startServer();
}).catch((error) => {
    console.log('THERE IS AN ERROR ON SERVER INITIALIZING');
    console.log(error);
});
